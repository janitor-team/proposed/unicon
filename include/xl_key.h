/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

/* key -- key processing module */

#ifndef	KEY_H
#define	KEY_H

/* special function key use to active input method */
#define NR_SPEC_KEY     18

#define CTRL_ALT_0      200
#define CTRL_ALT_1      201
#define CTRL_ALT_2      202
#define CTRL_ALT_3      203
#define CTRL_ALT_4      204
#define CTRL_ALT_5      205
#define CTRL_ALT_6      206
#define CTRL_ALT_7      207
#define CTRL_ALT_8      208
#define CTRL_ALT_9      209

#define CTRL_ALT_A      210
#define CTRL_ALT_X      211
#define CTRL_ALT_P      212
#define CTRL_ALT_N      213
#define CTRL_ALT_R      214

#define CTRL_SPACE      215
#define SHIFT_SPACE     216
#define SCROLL_LOCK     217

#define ALT_F1		220
#define CTRL_ALT_F1	220
#define ALT_F2		221
#define	CTRL_ALT_F2	221
#define ALT_F3		222
#define	CTRL_ALT_F3	222
#define ALT_F4          223
#define CTRL_ALT_F4     223
#define ALT_F5          224
#define CTRL_ALT_F5     224
#define ALT_F6          225
#define CTRL_ALT_F6     225
#define ALT_F7          226
#define CTRL_ALT_F7     226

/* system menu trigger */
#define CTRL_F5         227  
#define ALT_SPACE       228
#define F1_HELP         229

/* User Defined Phrase Support */
#define SHIFT_TAB       230

/* Hotkeys for switching. Refer xl_keymasks.h */
/* Rat: add 2001.07.31 */
#define CTRL_COMMA      231  //Half/FullChar Switch
#define CTRL_ALT_G      232  //Simplified Chinese
#define CTRL_ALT_B      233  //Tradtional Chinese
#define CTRL_ALT_J      234  //Japanese
#define CTRL_ALT_K      235  //Korean
#define CTRL_ALT_V      236  //Vietnamese
#define CTRL_LEFTSHIFT  237  //Circulate Inputmethods
#define CTRL_ALT_LEFTSHIFT  238  //Reverse Circulate Inputmethods
#define CTRL_PLUS 	239  //Define user words
#define KEY_UP		240  //Arrow Up
#define KEY_DOWN	241  //Arrow Down
#define KEY_LEFT	242  //Arrow Left
#define KEY_RIGHT	243  //Arrow Right
#define KEY_INS         244
#define KEY_DEL         245
#define KEY_HOME        246
#define KEY_END         247
#define KEY_PGU         248
#define KEY_PGD         249

void SetupKeymap(void);
void RestoreKeymap(void);
void KeymapInit(void);
void KeymapCleanup(void);
void ProcessNormalModeKey(int tty_fd,unsigned char c);

#endif
