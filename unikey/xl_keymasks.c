/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <linux/fs.h>
#include <linux/wrapper.h>

#include "xl_key.h"
#include "unikey.h"
#include "xl_keymasks.h"

int bFunKeyPressed = 0;
static int ctrl_pressed = 0, shift_pressed = 0, alt_pressed = 0;

int alt_shift_ctrl (unsigned char scancode)
{
#ifdef DEBUG
    printk ("keyboard::scancode = 0x%x\n", scancode);
#endif
    switch (scancode)
    {
        case LR_CTRL_KEY_DOWN:
             ctrl_pressed = 1;
             break;
        case LR_CTRL_KEY_UP:
             ctrl_pressed = 0;
             break;
        case L_SHIFT_KEY_DOWN:
             shift_pressed = 1;
             break;
        case L_SHIFT_KEY_UP:
             shift_pressed = 0;
             break;
        case L_ALT_KEY_DOWN:
             alt_pressed = 1;
             break;
        case L_ALT_KEY_UP:
             alt_pressed = 0;
             break;
        default:
             return 0;
    }
#ifdef DEBUG
    printk ("ctrl=%d, alt=%d, shift=%d\n",
                   ctrl_pressed,
                   alt_pressed,
                   shift_pressed);
#endif
    return 1;
}

int GetTotalIgnore (unsigned char scancode)
{
    static u_char table[] = 
    {
        0x1c, 0x35, 
        0x45, 0x47, 0x48, 0x49, 0x4b, 0x4d, 0x4f, 
        0x50, 0x51, 0x52, 0x53, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f,
        0x63, 
    };
    static int total = sizeof (table) / sizeof (u_char);
    int i;
    for (i = 0; i < total; i++)
        if (table[i] == scancode || 
            (table[i] + 0x80) == scancode)
            return 1;
    if (scancode == 0xe0 || scancode == 0xb7)
        return 2;
    return 0;
}

int ScancodeToKeycode (unsigned char scancode, 
                       unsigned char *keycode)
{
    static int right_shift_pressed = 0, 
               has_key_input_after_shift = 0;
    static int nkey = 0, multi_key = 0;
    static int bFunKeyPressed_flag = 0;

#ifdef DEBUG
    printk ("keyboard::scancode = 0x%x\n", scancode);
#endif
    if (bFunKeyPressed_flag == 1)
    {
        multi_key = GetTotalIgnore (scancode);
        bFunKeyPressed_flag = 0;
    }
    else if (scancode == 0xe0 || scancode == 0xe1)
    {
        multi_key = 0; 
        bFunKeyPressed = 1;
        bFunKeyPressed_flag = 1;
        nkey = 1;
        if (right_shift_pressed == 1)
            has_key_input_after_shift = 1;
        return 0;
    }
    if (bFunKeyPressed == 1 && multi_key != 0 && nkey <= multi_key)
    {
        nkey ++;
        return 0;
    }
    bFunKeyPressed = 0;
    nkey = multi_key = 0; 
    switch (scancode)
    {
        case R_SHIFT_KEY_DOWN: 
             right_shift_pressed = 1; 
             has_key_input_after_shift = 0;
             break;
        case R_SHIFT_KEY_UP: 
             right_shift_pressed = 0; 
             if (has_key_input_after_shift == 0)
             {
                 *keycode = CTRL_SPACE; 
                 has_key_input_after_shift = 0;
                 return 2;
             }
             has_key_input_after_shift = 0;
             break;
        case F5_KEY_DOWN:
             if (ctrl_pressed == 1 && alt_pressed != 1)
             {
                 *keycode = CTRL_F5;
                 return 4;
             }
             break;
        case F1_KEY_DOWN:
             if (ctrl_pressed == 1 && alt_pressed != 1)
             {
                 *keycode = F1_HELP;
                 return 4; 
             }
             break;
        case S_KEY_0_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_0;
                 return 4;
             }
             break;
        case S_KEY_1_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_1;
                 return 4;
             }
             break;
        case S_KEY_2_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_2;
                 return 4;
             }
             break;
        case S_KEY_3_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_3;
                 return 4;
             }
             break;
        case S_KEY_4_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_4;
                 return 4;
             }
             break;
        case S_KEY_5_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_5;
                 return 4;
             }
             break;
        case S_KEY_6_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_6;
                 return 4;
             }
             break;
        case S_KEY_7_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_7;
                 return 4;
             }
             break;
        case S_KEY_8_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_8;
                 return 4;
             }
             break;
        case S_KEY_9_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_9;
                 return 4;
             }
             break;
       case SPACE_KEY_DOWN:
             if (ctrl_pressed == 1)
             {
                 if (bHasClosed[nCurTty - 1] == 1)
                     bHasClosed[nCurTty - 1] = 0;
                 else
                     bHasClosed[nCurTty - 1] = 1;
                 OnTtyChangeUpdate (nCurTty-1);
                 return 3;
             }
             else if (shift_pressed == 1)
             {
                 *keycode = SHIFT_SPACE;
                 return 1;
             }
             break;
       case PERIOD_KEY_DOWN:
             if (ctrl_pressed == 1)
             {
                 *keycode = ALT_SPACE;
                 return 4;
             }
             break;
       case L_SHIFT_KEY_DOWN:
             if (ctrl_pressed == 1)
             {
                 *keycode = CTRL_LEFTSHIFT;
                 return 4;
             }
             break;
       case ALPHA_G_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_G;
                 return 4;
             }
             break;
       case ALPHA_B_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_B;
                 return 4;
             }
             break;
       case ALPHA_J_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_J;
                 return 4;
             }
             break;
       case ALPHA_K_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_K;
                 return 4;
             }
             break;
       case ALPHA_V_DOWN:
             if (ctrl_pressed == 1 && alt_pressed == 1)
             {
                 *keycode = CTRL_ALT_V;
                 return 4;
             }
             break;
       case COMMA_KEY_DOWN:
             if (ctrl_pressed == 1)
             {
                 *keycode = CTRL_COMMA;
                 return 4;
             }
             break;
       case PLUS_KEY_DOWN:
             if (ctrl_pressed == 1)
             {
                 *keycode = CTRL_PLUS;
                 return 4;
             }
             break;
      case TAB_KEY_DOWN:
             if (shift_pressed == 1)
             {
                 *keycode = SHIFT_TAB;
                 return 4; //Rat: what do those return values mean?
             }
    };
    if (right_shift_pressed == 1 && 
        !(scancode == R_SHIFT_KEY_UP || scancode == R_SHIFT_KEY_DOWN)) 
        has_key_input_after_shift = 1;
    else
        has_key_input_after_shift = 0;
    return 0;
}

