#!/bin/bash

if [ "$TARGET_KERNEL" = "" ] ; then
	TARGET_KERNEL=`uname -r` 
fi

if [ -d /lib/modules/$TARGET_KERNEL/kernel ]; then 
	INSTALLPATH=/lib/modules/$TARGET_KERNEL/kernel/drivers/misc 
else 
        INSTALLPATH=/lib/modules/$TARGET_KERNEL/misc 
        echo INSTALLPATH=$INSTALLPATH 
fi

mkdir -p $INSTALLPATH
echo installing $1 to $INSTALLPATH
install -o root -g root -m644 $1 $INSTALLPATH
