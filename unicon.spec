#$Id: unicon.spec,v 1.2 2001/08/13 15:04:52 linuxrat Exp $
%define nam  unicon
%define ver  3.0.4
%define rel  3
%define rvd  20010911
%define srl  2001091101

Name: %{nam}
Version: %{ver}
Release: %{rel}
Serial: %{srl}
BuildRoot: /var/tmp/%{name}-%{version}-root

Summary: UNICON -- UNIversal CONsole for CJKV display & input.
Summary(zh_CN): UNICON - GNU/Linux 多语言虚拟控制台主程序．
License: GPL
Group: System Environment/Base
Source0: %{name}-%{version}-%{rvd}.tar.bz2
BuildRequires: pth

#NoSource: 0

%package utils
Summary: Utilities for UNICON.
Summary(zh_CN): UNICON 实用工具包.
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description
This Project is intend to implement display/input CJKV
(Chinese/Japanese/Korean/Vietnamese) characters under 
Framebuffer Console Mode within GNU/Linux System.

%description(zh_CN)
UNICON 是一个 GNU/Linux 系统中内核级的多语言虚拟控制台, 支持
繁简体中文，日文，韩文，越南文的显示和输入. 具有良好的兼容性
和稳定性．目前仅支持中文输入，日文韩文输入正在移植当中，越南
文的显示输入将在未来得到支持．

%description utils
UNICON additional utilities, include language selection program etc.

%description utils
UNICON 的附加工具, 包括语言选择工具等.

%prep
%setup -q

%build
./configure --prefix=/usr
make
make data
make -C sfonts/tools

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
make install prefix=$RPM_BUILD_ROOT/usr
install -s -m755 sfonts/tools/sfont $RPM_BUILD_ROOT/usr/bin/
install -s -m755 tools/uniconcfg $RPM_BUILD_ROOT/usr/bin/
install -s -m755 tools/uniconctrl $RPM_BUILD_ROOT/usr/bin/
make data-install prefix=$RPM_BUILD_ROOT/usr

install -m755 scripts/unicon-start $RPM_BUILD_ROOT/usr/bin/unicon-start

mkdir -p $RPM_BUILD_ROOT/etc/rc.d/init.d
install -m755 scripts/unicon-init $RPM_BUILD_ROOT/etc/rc.d/init.d/unicon

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc doc BUGS COPYING CREDITS ChangeLog INSTALL* RELEASE* README* THANKS
/usr/lib/unicon
/usr/bin/unicon
/usr/bin/unicon-start
/etc/rc.d/init.d/unicon

%files utils
/usr/bin/sfont
/usr/bin/uniconcfg
/usr/bin/uniconctrl

%changelog
* Sat Sep 8 2001 James Su <suzhe@turbolinux.com.cn>
- Fixed the bugs in Internal Code input module.

* Fri Aug 24 2001 Merlin Ma <merlin@turbolinux.com.cn>
- update to 3.0.4

* Wed Jul 18 2001 Merlin Ma <merlin@turbolinux.com.cn>
- updated init script and start script
- fixed a few startup and unload problem.

* Thu Jul 12 2001 TAKEDA Hirofumi <takepin@turbolinux.co.jp>
- added securlevel in init script

* Thu May 7 2001 Merlin Ma <merlin@turbolinux.com.cn>
- update to 3.0.3-release
- fix spec file

* Wed Mar 28 2001 Merlin Ma <merlin@turbolinux.com.cn>
- fix zhuyin symbol display problem from Simon.
- other minor bugfix.

* Mon Feb 27 2001 Merlin Ma <merlin@turbolinux.com.cn>
- Minor document update.

* Mon Feb 25 2001 Merlin Ma <merlin@turbolinux.com.cn>
- make unicon as system service in /etc/rc.d/init.d .

* Fri Feb 22 2001 Merlin Ma <merlin@turbolinux.com.cn>
- add unicon start script. 
- Update to 3.0.2-release

* Mon Feb 19 2001 Merlin Ma <merlin@turbolinux.com.cn>
- Update to 3.0.2-4
- Add CXterm IM module.
- Add IMM dicts for Big5

* Wed Feb 14 2001 Merlin Ma <merlin@turbolinux.com.cn>
- Update to 3.0.1 release
- rebuild patch for 3.0.1
- rename miniuni to uniconctrl
- split unicon to 2 package : unicon , unicon-utils
- fix some name problem in spec file.
- add more text in %doc section

* Sat Jan 27 2001 Koushi Takahashi <koushi@turbolinux.co.jp>
- Changed NoSource: tag

* Fri Dec 8 2000 Go Taniguchi <go@turbolinux.co.jp>
- update 3.0-release
- added miniuni amd unicon-mod

* Mon Oct 16 2000 Go Taniguchi <go@turbolinux.co.jp>
- remove sfonts source, move to kernel sources

* Fri Oct 13 2000 Go Taniguchi <go@turbolinux.co.jp>
- initial release

