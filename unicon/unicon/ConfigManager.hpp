/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __CONFIGMANAGER_HPP__
#define __CONFIGMANAGER_HPP__

#include "MyConfig.hpp"

#define  MAX_CODE             5
#define  MAX_TTY              6

/* font_type */
#define XL_DB_GB       0
#define XL_DB_BIG5     1
#define XL_DB_JIS      2
#define XL_DB_KSCM     3
#define XL_DB_GBK      4

class CConfigManager
{
private:
    int   coding[MAX_TTY];
    CMyConfig *cfg[MAX_CODE];
public:
    CConfigManager (char *szConfigName, int DefaultCoding);
    ~CConfigManager ();
    int GetTtyCoding (int tty);
    int SetTtyCoding (int tty, int coding);
    CMyConfig *GetTtyConfig (int tty);
};

#endif

