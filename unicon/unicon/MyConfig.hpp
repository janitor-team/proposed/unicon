/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __MYCONFIG_HPP__
#define __MYCONFIG_HPP__

#include <fstream.h>
#include <SysConfig.hpp>

#define NR_INPUT           20

struct ImmOpMethod
{
     char *szMethodName;
     char *szImmModule;
     char *szImmTable;
     long type;
};

class CMyConfig
{
private:
    char *szConfigFileName;
    char *szLanguageName;
public:
    /* Input Method Module defination */
    ImmOpMethod aInputMethod[NR_INPUT];
    int TotalMethod;
    /* IMM User Interface  defination */
    char *szFullSymbolLeftMark,
         *szFullSymbolRightMark,
         *szSingleSymbolLeftMark,
         *szSingleSymbolRightMark;
    char *szFullChar,
         *szSingleChar,
         *szSingleSymbol,
         *szFullSymbol,
         *szVersionInfo,
         *szLanguageCode,
         *szFirstMessage;
    /* Help defination */
    char **pszHelp;
    int  TotalHelp;

    /* SysMenu Support */
    char *szSysMenu;
private:
    long GetLanguageCode (char *szLanguageCode);
    void FreeString (char *p);
    char *szLoadSysConfigString (CSysConfig *pConfig, char *p, 
                                 char *szCapName, char *szDefault);
    bool LoadLanguageCode ();
public:
    CMyConfig (char *szConfigFileName, char *szLanguageName);
    ~CMyConfig ();
    bool ChangeLanguageCode (char *szNewLanguageCode);
    char *szGetCurrentLanguageCode ();
    void PrintAll ();
};

#endif

