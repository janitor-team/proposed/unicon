/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __USERPHRASE_HPP__
#include <Phrase.hpp>

#define USERPHRASEFILEMARK         "UserPhraseTable"

class CUserPhrase
{
public:
    CPhrase *pCPhrase;
    CMemPhrase *pCMemPhrase;
private:
    bool PhraseItemToBuf (PhraseItem *Item, u_char *buf,
                          u_short maxlen, u_short *len);
    bool bBufToPhraseItem(u_char *buf, u_short len,
                          PhraseItem *Item);
public:
    CUserPhrase (char *szFileName = NULL);
    ~CUserPhrase ();
    bool SaveUserPhrase (char *szFileName);
    void AppendPhrase (PhraseItem *Item);
    u_long GetTotalItem ();
};

#endif

