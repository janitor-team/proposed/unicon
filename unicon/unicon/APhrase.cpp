
/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include <APhrase.hpp>

CAPhrase::CAPhrase (CMemPhrase *pCMemPhrase0, 
                    u_long nInitMaxItem, 
                    u_long DynIncreaseStep)
{
    pCMemPhrase  = pCMemPhrase0;
    ppPhraseItem = (PhraseItem **) 
                  malloc (nInitMaxItem * sizeof (PhraseItem *));
    if (ppPhraseItem == (PhraseItem **) NULL)
    {
        printf ("No enough memory to run in CAPhrase ()\n");
        exit (0);
    }
    MaxItem = nInitMaxItem;
    nCurItem = 0;
    DynIncStep = DynIncreaseStep;
}

CAPhrase::~CAPhrase ()
{
    free (ppPhraseItem);
}

void CAPhrase::AddPhraseItem (PhraseItem *p)
{
    if (nCurItem >= MaxItem)
    {
        MaxItem += DynIncStep;
        ppPhraseItem = (PhraseItem **) realloc 
                      (ppPhraseItem, MaxItem * sizeof (PhraseItem *));
        if (ppPhraseItem == (PhraseItem **) NULL)
        {
            printf ("No enough memory to run in CAPhrase ()\n");
            exit (0);
        }
    }   
    if (pCMemPhrase  != NULL)
        ppPhraseItem[nCurItem++] = pCMemPhrase->pAddPhraseItem (p);
    else
        ppPhraseItem[nCurItem++] = p;
}

PhraseItem * CAPhrase::pGetPhraseItem (u_long n)
{
    assert (n < nCurItem);
    return ppPhraseItem[n];
}

u_long CAPhrase::GetTotalItem ()
{
    return nCurItem;
}

static int compareByFreq (const void *a1, const void *a2)
{
    const PhraseItem *p1 = (PhraseItem *) a1;
    const PhraseItem *p2 = (PhraseItem *) a2;
    if (*p1->frequency > *p2->frequency)
        return 1;
    else if (*p1->frequency < *p2->frequency)
        return -1;
    else
        return 0;
}

static int compareByKeys (const void *a1, const void *a2)
{
    const PhraseItem *p1 = (PhraseItem *) a1;
    const PhraseItem *p2 = (PhraseItem *) a2;
    return strcmp (p1->szKeys, p2->szKeys);
}

void CAPhrase::SortByFreq ()
{
    qsort (ppPhraseItem, nCurItem, sizeof (PhraseItem *), compareByFreq);
}

void CAPhrase::SortByKeys ()
{
    qsort (ppPhraseItem, nCurItem, sizeof (PhraseItem *), compareByKeys);
}

