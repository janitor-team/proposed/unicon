/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <stdio.h>
#include <assert.h>

#include "ConfigManager.hpp"

CConfigManager::CConfigManager (char *szConfigName, int DefaultCoding)
{
    int i;
    static char *code[5] = { "GB", "BIG5", "JIS", "KSCM", "GBK"};
    for (i = 0; i < MAX_CODE; i++)
        cfg[i] = new CMyConfig (szConfigName, code[i]);
    for (i = 0; i < MAX_TTY; i++)
        coding[i] = DefaultCoding;
    // for (i = 0; i < MAX_CODE; i++)
    //    cfg[i]->PrintAll ();
}

CConfigManager::~CConfigManager ()
{
    for (int i = 0; i < MAX_CODE; i++)
        delete cfg[i];
}

int CConfigManager::GetTtyCoding (int tty)
{
//    printf ("int CConfigManager::GetTtyCoding (%d) \n", tty);
//    printf ("    return %d\n", coding[tty]);
    assert (tty >= 0 && tty < MAX_TTY);
    return coding[tty];
}

int CConfigManager::SetTtyCoding (int tty, int thiscoding)
{
    assert (tty >= 0 && tty < MAX_TTY);
    assert (thiscoding >= 0 && thiscoding < MAX_CODE);

//  printf ("int CConfigManager::SetTtyCoding (%d,%d)\n", tty, thiscoding);
//  printf ("    coding[%d] = %d\n", tty, thiscoding);

    int old = coding[tty];
    coding[tty] = thiscoding;
    return old;
}

CMyConfig * CConfigManager::GetTtyConfig (int tty)
{
    assert (tty >= 0 && tty < MAX_TTY);
//    printf ("CMyConfig * CConfigManager::GetTtyConfig (%d)\n", tty);
    int c = coding[tty];
//    printf ("CMyConfig * CConfigManager::cfg[%d]\n", c);
    return cfg[c];
}

