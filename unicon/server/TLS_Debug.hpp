/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __DEBUG_HPP__
#define __DEBUG_HPP__

#include <stdio.h>
#include <stdlib.h>

typedef char * PSTR;
class TLS_CDebug
{
private:
   FILE *fp;
   bool bDebugToFile;
public:    
    TLS_CDebug (char *szFileName = NULL, /* NULL -- screen dump */
            int mode = 0);           /* 0    -- over write 
                                        1    -- append */
    ~TLS_CDebug ();
    /* printf support */
    int printf (const char *format, ...);

    /* operator overload */
    friend TLS_CDebug & operator << (TLS_CDebug &in, long b);
    friend TLS_CDebug & operator << (TLS_CDebug &in, u_long b);

    friend TLS_CDebug & operator << (TLS_CDebug &in, short b);
    friend TLS_CDebug & operator << (TLS_CDebug &in, u_short b);

    friend TLS_CDebug & operator << (TLS_CDebug &in, char b);
    friend TLS_CDebug & operator << (TLS_CDebug &in, u_char b);

    friend TLS_CDebug & operator << (TLS_CDebug &in, PSTR p);
};

#endif


