/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __DOUBLEBYTECONVERTOR_HPP__
#define __DOUBLEBYTECONVERTOR_HPP__

class TLS_CDoubleByteConvertor
{
public:
    int IsGB2312(u_char ch1, u_char ch2);
    int IsBIG5 (u_char ch1, u_char ch2);
    void GbCharToBig5(char *gb, char *big5);
    void Big5CharToGb (char *big5, char *gb);
    void GbStringToBig5String (char *gbstr, char *big5str, int n);
    void Big5StringToGbString (char *big5str, char *gbstr, int n);
public:
    TLS_CDoubleByteConvertor ();
    ~TLS_CDoubleByteConvertor ();
    int String (char *szStr, long SourceType, long TargetType);
    int String2 (char *szStr1, long SourceType, 
                char *szStr2, long TargetType);
};

#endif

