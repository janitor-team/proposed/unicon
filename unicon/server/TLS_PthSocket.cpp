/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <assert.h>
#include <TLS_PthSocket.hpp>
#include <pth.h>

TLS_CPthSocket::TLS_CPthSocket (int fd): fd(fd)
{
}

TLS_CPthSocket::~TLS_CPthSocket ()
{
}

int TLS_CPthSocket::PollRead (char *buf, int len)
{
    char *p = buf;
    int b = len, t;
    fd_set readset;
    struct timeval timeout;

    do
    {
        FD_ZERO (&readset);
        FD_SET (fd, &readset);
        timeout.tv_sec = 120;
        timeout.tv_usec = 0;

        if (select (fd+1, &readset, NULL, NULL, &timeout) <= 0)
           continue;
        t = pth_read_ev(fd, p, len, NULL);
        if (t < 0)
           continue;
        p += t;
        len -= t;
    }
    while (len != 0);
    return b;
}

int TLS_CPthSocket::PollWrite (char *buf, int len)
{
    char *p = buf;
    int b = len, t;

    do
    {
        t = pth_write_ev(fd, p, len, NULL);
        if (t < 0)
           continue;
        p += t;
        len -= t;
    }
    while (len != 0);
    return b;
}

int TLS_CPthSocket::read (void *buf, int buflen)
{
    short len;
#ifdef __IMM_DEBUG__
    printf ("receiving:");
#endif
    PollRead ((char *) &len, sizeof (short));
    assert (len < buflen);
    PollRead ((char *) buf, len);
#ifdef __IMM_DEBUG__
    char *p = (char *) buf;
    printf ("len = %d\n", len);
    for (int i = 0; i < len; i++)
        printf ("(0x%x,%c)", p[i], p[i]);
    printf ("\n");
#endif
    return len;
}

int TLS_CPthSocket::write (void *buf, int len)
{
    short len0 = len;
#ifdef __IMM_DEBUG__
    printf ("sending: len = %d\n", len);
#endif
    PollWrite ((char *) &len0, sizeof (short));
    PollWrite ((char *) buf, len);
#ifdef __IMM_DEBUG__
    char *p = (char *) buf;
    for (int i = 0; i < len; i++)
        printf ("(0x%x,%c)", p[i], p[i]);
    printf ("\n");
#endif
    return len;
}
 
