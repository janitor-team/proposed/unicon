/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2002
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __XL_PHRASE_H__
#define __XL_PHRASE_H__

void SortPhraseItem (TL_SysPhrase_T *p, hz_input_table *cur_table);
int ResortPhraseFreq (HzInputTable_T *pClient);
void LoadPhrase (HzInputTable_T *pClient, int phrno, char *tt);
long GetAssociatePhraseIndex (HzInputTable_T *pClient,
                              int index, u_long *nPhrase);
int IsThisPhraseExist 
            (HzInputTable_T *p, char *szCode, char *szPhrase);
int AppendPhrase 
            (HzInputTable_T *p, char *szCode, char *szPhrase);
int DeletePhrase (HzInputTable_T *p, char *szPhrase);
int AdjustPhraseOrder (HzInputTable_T *pClient, long nPhrase);
int ResortPhraseFreq (HzInputTable_T *pClient);
int FindAssociateKey (HzInputTable_T *pClient, u_char *pStr);
hz_input_table* LoadInputMethod(char *filename);
void UnloadInputMethod (hz_input_table *p);
int SaveLoadInputMethod (hz_input_table *cur_table, char *filename);
int DumpLoadInputMethod (HzInputTable_T *p, char *filename);
#endif

