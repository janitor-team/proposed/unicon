#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

FILE *fr, *fw;
int lineno;

void print_error(char *fmt,...)
{
  va_list args;

  va_start(args, fmt);
  vfprintf(stderr, fmt, args);
  va_end(args);
  fprintf(stderr,"\n");
  exit(-1);
}

char *skip_space(char *s)
{
        while ((*s==' ' || *s=='\t') && *s) s++;
        return s;
}

char *to_space(char *s)
{
        while (*s!=' ' && *s!='\t' && *s) s++;
        return s;
}

void del_nl_space(char *s)
{
        char *t;

        int len=strlen(s);
        if (!*s) return;
        t=s+len-1;
        while ( (*t=='\n' || *t==' ' || *t=='\t' )&& s<t) t--;
        *(t+1)=0;
}

int get_line (u_char *tt)
{
        if (feof(fr))
            return 0;
        while (!feof(fr))
        {
                fgets(tt,128,fr);
                lineno++;
                if (tt[0]=='#') continue;
                else break;
        }
        return 1;
}

void del_enter (char *s)
{
   int n = strlen (s);
   while (n != 0)
   {
      if ( !(s[n] == '\r' || s[n] == '\n' || s[n] == '\0'))
         break;
      n --;
   }
   s[n+1] = '\0';
}

void SpliteTwoLine (u_char *buf, u_char *code, u_char *phrase)
{
    int i, len = strlen (buf);
    
    for (i = 0; i < len; i += 2)
    {
        if (buf[i] < 0x80)
            break;
        *phrase++ = buf[i];
        *phrase++ = buf[i+1];
    }
    *phrase = '\0';
    for (; i < len; i++)
        *code++ = buf[i];
    *code = '\0';
}

int main (int argc, char **argv)
{
    FILE *fp, *fpOut;
    u_char buf[128];
    u_char code[32], phrase[128];
    if (argc != 3)
    {
        printf ("Usage:%s <microsoft mabiao> <target file> \n");
        exit (0);
    }
    fp = fopen (argv[1], "rt");
    fr = fp;
    fpOut = fopen (argv[2], "wt");
    while (get_line (buf) == 1)
    {
        del_enter (buf);
        if (buf[0] > 0x80)
        {
           SpliteTwoLine (buf, code, phrase);
           fprintf (fpOut, "%s %s\n", code, phrase);
        }
    } 
    fclose (fp);
    fclose (fpOut);
}

