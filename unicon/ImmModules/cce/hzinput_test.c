/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2002
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

/*
   gcc -g -I../include -I. -D__CCE_HZINPUT_DEBUG__ xl_hzinput.c CCE_hzinput.c
   gcc -I../../include -I. test.c xl_hzinput.c CCE_hzinput.c
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xl_hzinput.h>
#include <ImmModule.h>
#include <unistd.h>


int
main ()
{
  struct ImmOperation ImmOp_Ptr;
  IMM_CLIENT *pImm;
  char buf1[64];
  char *szTest = "kexue ";	//ng";
  int i, len;
  long n;
  char *tblfile = "inputs/pinyin.tab";

  if (access (tblfile, R_OK) != 0)
    printf ("Sorry, I can't read %s.\n", tblfile), exit (-1);

  pImm = ImmOp_Ptr.open (tblfile, 0);
  pImm->m.szPhrase = pImm->buf;

  printf ("\n");
  for (i = 0; i < strlen (szTest); i++)
    {
      char buf[256];
      n = ImmOp_Ptr.KeyFilter (pImm, szTest[i], buf1, &len);
      ImmOp_Ptr.GetInputDisplay (pImm, buf, sizeof (buf)),
	printf ("Input::%s\n", buf);
      ImmOp_Ptr.GetSelectDisplay (pImm, buf, sizeof (buf));
      printf ("Selection::%s\n", buf);

      if (n == 2)
	printf ("you select::%s\n", buf1);
      printf ("\n");
    }
  printf ("\n");
  ImmOp_Ptr.close (pImm);
  return 1;
}
