/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2002
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

/* Imm Standard Interfaces */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xl_hzinput.h>
#include <ImmModule.h>

extern int CCE_KeyFilter (HzInputTable_T * p, u_char key, char *buf,
			  int *len);
extern char *szGetSelItem (HzInputTable_T * pClient, int vv);
extern hz_input_table *CCE_LoadMethod (char *szFileName);
extern void CCE_InputInit (HzInputTable_T * p);
extern int CCE_UnloadMethod (hz_input_table * q);
extern int CCE_ConfigureInputArea (HzInputTable_T * p, int SelectionLen);
extern int CCE_GetInputDisplay (HzInputTable_T * p, char *buf);
extern int CCE_GetSelectDisplay (HzInputTable_T * p, char *buf);

void
SetPhraseBuffer (PhraseItem * p, char *buf, int buflen)
{
  char *p1 = buf;

  p->szKeys = p1;
  p1 += 32;

  p->KeyLen = (u_char *) p1;
  p1 += sizeof (*p->KeyLen);

  p->frequency = (freq_t *) p1;
  p1 += sizeof (freq_t);

  p->szPhrase = p1;
}

static IMM_CLIENT *
IMM_open (char *szFileName, long type)
{
  HzInputTable_T *p;
  IMM_CLIENT *q;
  hz_input_table *a;

  a = CCE_LoadMethod (szFileName);
  if (a == NULL)
    return NULL;

  p = (HzInputTable_T *) malloc (sizeof (HzInputTable_T));
  if (p == NULL)
    return NULL;

  q = (IMM_CLIENT *) malloc (sizeof (IMM_CLIENT));
  if (q == NULL)
    {
      free (p);
      free (a);
      return NULL;
    };
  SetPhraseBuffer (&q->m, q->buf, sizeof (q->buf));
  q->pImmClientData = (void *) p;
  CCE_InputInit (p);
  p->cur_table = a;
  return q;
}

static int
IMM_save (IMM_CLIENT * p, char *szFileName)
{
  return 1;
}

static int
IMM_close (IMM_CLIENT * p)
{
  HzInputTable_T *pClient = (HzInputTable_T *) p->pImmClientData;
  CCE_UnloadMethod (pClient->cur_table);
  free (p->pImmClientData);
  free (p);
  return 1;
}

/* Indepent Modules support */
static int
IMM_KeyFilter (IMM_CLIENT * p, u_char key, char *buf, int *len)
{
  return CCE_KeyFilter ((HzInputTable_T *) p->pImmClientData, key, buf, len);
}

/* Input Area Configuration & Operation */
static int
IMM_ConfigInputArea (IMM_CLIENT * p, int SelectionLen)
{
  return CCE_ConfigureInputArea ((HzInputTable_T *) p->pImmClientData,
				 SelectionLen);
}

static int
IMM_GetInputDisplay (IMM_CLIENT * p, char *buf, long buflen)
{
  return CCE_GetInputDisplay ((HzInputTable_T *) p->pImmClientData, buf);
}

static int
IMM_GetSelectDisplay (IMM_CLIENT * p, char *buf, long buflen)
{
  return CCE_GetSelectDisplay ((HzInputTable_T *) p->pImmClientData, buf);
}

int
IMM_ResetInput (IMM_CLIENT * p)
{
  char buf[32];
  int len;
  /* Send Esc */
  CCE_KeyFilter ((HzInputTable_T *) p->pImmClientData, '\033', buf, &len);
  return 1;
}

PhraseItem *
IMM_pGetItem (IMM_CLIENT * p, u_long n)
{
  HzInputTable_T *pClient = (HzInputTable_T *) p->pImmClientData;
  char *s = szGetSelItem (pClient, (int) n);
  if (s == NULL)
    return NULL;
  strcpy (p->m.szPhrase, s);
  return &p->m;
}

/* Phrase Operation */
static int
IMM_AddPhrase (IMM_CLIENT * pClient, PhraseItem * p)
{
  return 1;
}

static int
IMM_ModifyPhraseItem (IMM_CLIENT * p, long n, PhraseItem * pItem)
{
  return 1;
}

static int
IMM_Flush ()
{
  return 1;
}

#ifdef  __cplusplus
extern "C"
#endif
struct ImmOperation ImmOp_Ptr = {
  "ƴ��",
  "CCE Chinese Input Version 1.0",
  "Author:He Rui",
  IMM_CCE | IMM_LC_GB2312 << 24,
  IMM_open,
  IMM_save,
  IMM_close,

  /* Indepent Modules support */
  IMM_KeyFilter,
  IMM_ResetInput,

  /* Input Area Configuration & Operation */
  IMM_ConfigInputArea,
  IMM_GetInputDisplay,
  IMM_GetSelectDisplay,
  IMM_pGetItem,
  IMM_AddPhrase,
  IMM_ModifyPhraseItem,
  IMM_Flush,
};

#ifdef __CCE_HZINPUT_DEBUG__
int
test (long a, PhraseItem * p)
{
  printf ("%s\n", p->szPhrase);
  return 1;
}

/*
    gcc -g -I../include -I. -D__CCE_HZINPUT_DEBUG__ xl_hzinput.c CCE_hzinput.c
*/

#include <unistd.h>

int
main ()
{
  IMM_CLIENT *pImm;
  char *szTest = "wa";		//ng";
  int i, j;
  long n;
  PhraseItem *p;
  PhraseItem a;
  char *tblfile = "./inputs/pinyin.tab";


  if (access (tblfile, R_OK) != 0)
    printf ("Sorry, I can't read %s.\n", tblfile), exit (-1);

  pImm = ImmOp_Ptr.open (tblfile);	///pinyin.tab");
  //"../../datas/ziranma.tab");
  pImm->m.szPhrase = pImm->buf;

  printf ("\n");
  for (i = 0; i < strlen (szTest); i++)
    {
      char buf[256];
      n = ImmOp_Ptr.KeyFilter (pImm, szTest[i]);
      ImmOp_Ptr.GetInputDisplay (pImm, buf, sizeof (buf)),
	printf ("Input::%s\n", buf);
      ImmOp_Ptr.GetSelectDisplay (pImm, buf, sizeof (buf));
      printf ("Selection::%s\n", buf);

      printf ("each select test...\n");
      for (j = 0; j < n; j++)
	{
	  p = IMM_pGetItem (pImm, j);
	  if (p != NULL)
	    printf ("%s,", p->szPhrase);
	}
      printf ("\n");
    }

  p = ImmOp_Ptr.DoSelectItem (pImm, 0);
  if (p != NULL)
    printf ("you select : 0 ==> %s\n", p->szPhrase);
  printf ("resuming....\n");
  n = ImmOp_Ptr.RestoreToLastStep (pImm);
  for (j = 0; j < n; j++)
    {
      p = IMM_pGetItem (pImm, j);
      if (p != NULL)
	printf ("%s,", p->szPhrase);
    }
  printf ("\n");

  ImmOp_Ptr.PageUp (pImm);
  ImmOp_Ptr.PageDown (pImm);

  ImmOp_Ptr.close (pImm);
  return 1;
}
#endif
