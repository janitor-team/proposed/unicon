/*
Compilation:
  gcc -g -I../../include -I. intcode_test.c intcode.c -o intcode_test
Run:
  ./intcode_test
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <xl_hzinput.h>
#include <ImmModule.h>


extern struct ImmOperation ImmOp_Ptr;
int
test (IMM_CLIENT * pImm, char *inbuf, char *type)
{
  int i, len;
  long n;
  char buf1[64];

  printf ("Charset: %s\n", type);
  for (i = 0; i < strlen (inbuf); i++)
    {
      char buf[256];
      n = ImmOp_Ptr.KeyFilter (pImm, inbuf[i], buf1, &len);
      ImmOp_Ptr.GetInputDisplay (pImm, buf, sizeof (buf)),
	printf ("Input::%s\n", buf);
      ImmOp_Ptr.GetSelectDisplay (pImm, buf, sizeof (buf));
      printf ("Selection::%s\n", buf);

      if (n == 2)
	printf ("you select::%s\n", buf1);
      printf ("\n");
    }

  return 0;
}

int
main ()
{
  struct ImmOperation ImmOp_Ptr;
  IMM_CLIENT *pImm;
  char *szTest1 = "8140 ";
  char *szTest2 = "81308139 ";

  pImm = ImmOp_Ptr.open (NULL, 0);
  pImm->m.szPhrase = pImm->buf;

  printf ("\n");
  test (pImm, szTest1, "GBK");
  test (pImm, szTest2, "GB18030");
  printf ("\n");
  ImmOp_Ptr.close (pImm);
  return 1;
}
