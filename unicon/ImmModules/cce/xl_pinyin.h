/*
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2002
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 *
 * $Id$
 */

#ifndef	_INPUT_MODULE_H_
#define	_INPUT_MODULE_H_

#define MAX_PY_NUM 410
 /* 1-405 */
#define MAX_EACH_PY 38
#define MAX_EACH_HZ 241
#define MAX_PY_LEN 7
#define MAX_SELECT_PH 1000
#define MAX_RES_NAME_LEN 60
#define MAX_CHAR_NUM 10
#define MAX_INPUT_BUF 100
#define MAX_HZ_BUF 250

#define MAX_PHRASE_LEN 8
#define MAX_PHRASE_COUNT 250
  /* same keyphrase phrase count, 106 in fact */

//#define SEL_AREA_WIDTH 50
#define PINYIN_AREA_WIDTH 20

/* Max single char pinyin length: 6, zhuang, chuang ... */
typedef int bool;
#define true     1
#define false    0
#define SUCCESS  0

typedef struct
{
  u_short key;
  char py[MAX_PY_LEN];		// 7 + 2 = 9 bytes
}
PinYin;

/* UsrPhrase is a linked list structure */
typedef struct _UsrPhrase
{
  struct _UsrPhrase *next;
  u_char len;			// phrase len
  u_char count;			// phrase number
  u_char key[0];		// pinyin key [len+1]
}
UsrPhrase;

#define SizeOfPhrase(len,count)  ( 2 + (len)+1 + (count)*((len)*2+1) )

typedef struct _Phrase
{
  u_char len;			// phrase len
  u_char count;			// phrase number
  u_char key[0];		// pinyin key [len+1]
}
Phrase;

typedef struct _SysPhrase
{
  u_short count;
  Phrase phrase[0];
}
SysPhrase;

typedef struct _ChoiceItem
{
  Phrase *head;			// pointer to the len field
  u_char index;			// index of the phrase in that 
}
ChoiceItem;

typedef char PYString[MAX_PY_LEN];

typedef struct _InputModule
{
/*
  PinYin pytab[26][MAX_EACH_PY];
      // MAX_EACH_PY = 38    a[], b[], c[] ....
      // map the pinyin to keys
  SysPhrase *sysph[MAX_PY_NUM];   // system phrases
  int sys_size;

  int FuzzyPinyin;   // zh-ch-sh z-c-s
  UsrPhrase *usrph[MAX_PY_NUM];  //user defined phrase
*/
  char inbuf[MAX_INPUT_BUF];	/* input buffer for keystrokes */
  // whole inputed pinyin string
  char inbuftmp[MAX_INPUT_BUF];
  // un-selected pinyin string
  char pybuftmp[MAX_INPUT_BUF];
  // selected pinyin string, hanzi

  PYString pinyin[2 * MAX_PHRASE_LEN];
  // MAX_PY_LEN = 7, MAX_PHRASE_LEN = 6
  int lenpy;
  // current total pinyin len
  int pinyinpos;
  // current pinyin position

  u_char key[MAX_PHRASE_LEN + 1];
  int lenkey;			// the pinyin keys of current selected phrase

  ChoiceItem sel[MAX_SELECT_PH];	// final selection
  Phrase *tempsel[MAX_PHRASE_LEN][500];	// temperoray usage
  int seltotal[MAX_PHRASE_LEN];

  int len;			// total selectable char/phrases
  int startpos;
  int endpos;			// startpos and endpos of showed selection

  char iapybuf[MAX_INPUT_BUF];
  // selected hanzi and inputed pinyin area
  char iahzbuf[MAX_HZ_BUF];
  // MAX_HZ_BUF = 250
  int nTotalCurSel;		/* Total Selection */
  int SelectionLen;
}
InputModule;			// about 30KB


/* PinYin input */

int InitPinyinInput (char *szPath);
void PinyinInputCleanup ();
void ResetPinyinInput (InputModule * inmd);
int Pinyin_KeyFilter (InputModule * inmd, u_char key, char *buf, int *len);
int Pinyin_KeyPressed (InputModule * inmd, u_char key);
void RefreshPYInputArea (InputModule * inmd);
int UnloadSysPhrase ();
int UnloadUserPhrase ();

#endif
