#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>
#include <pwd.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <stdlib.h>

#include "hzinput.h"

void
init()
{
      //refresh_input_method_area();
  hz_input_init();
/*
  int i;
  for(i=0; i<NR_INPUTMETHOD; i++){
	char fname[30];
	sprintf(fname, "./dict/%d", i);
	load_input_table(i, fname);
  }
*/
}

void 
run(void)
{
  //toggle_input_method();
  refresh_input_method_area();
  //set_active_input_method(0);
  hz_filter('d');
  hz_filter('i');
  hz_filter('\010');
  hz_filter('\010');
  hz_filter('t');
  hz_filter('i');
  hz_filter('n');
  hz_filter('g');
  hz_filter('d');
  hz_filter('>');
  hz_filter('3');
  hz_filter('p');

  set_active_input_method(1);
  hz_filter('f');
  hz_filter('1');
  hz_filter('g');
  hz_filter('2');
	//load_input_table(i, s2);
	//unload_input_table(i);
}

void
done(void)
{
  hz_input_done();
}

void main(void)
{
  init();
  run();
  done();  
}


