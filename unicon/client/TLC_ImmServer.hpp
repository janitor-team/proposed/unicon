
/*
**  UNICON - The Console Chinese & I18N 
**  Copyright (c) 1999-2002 Arthur Ma <arthur.ma@turbolinux.com.cn>
**
**  This file is part of UNICON, a console Chinese & I18N
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This library is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  Lesser General Public License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
**
** TLC_ImmServer.hpp: Imm Server interface
** See the file COPYING directory of this archive
** Author: see CREDITS
*/

/*****************************************************************************
 *
 * Communicaton.h  ===  Communicaton between Server and Client 
 *
 *****************************************************************************/
#ifndef __IMMSERVER_HPP__
#define __IMMSERVER_HPP__
#include <stdlib.h>
#include <ImmClient.h>
#include <TLC_SocketClient.hpp>
#include <TLC_MemFile.hpp>

class TLC_CImmServer
{
private:
    TLC_CSocketClient *pCSocketClient;
    TLC_CMemFile *MemIn, *MemOut;
public:
    TLC_CImmServer (char *szIpAddr, u_short port);
    ~TLC_CImmServer ();

    /* Server Imm Operations */
    IMM_HANDLE  OpenImm (char *szImmModule,
                         char *szImmTable, u_long type, IMM *p);
    int CloseImm (IMM_HANDLE  handle);

    /* Imm Input Method Operations */
    int ResetInput (IMM_HANDLE  handle);
    int KeyFilter (IMM_HANDLE handle, u_char key, char *buf, int *len);
    int SetInputMode  (IMM_HANDLE handle, long mode);

    /* Input Area Configuration & Operation */
    int ConfigInputArea (IMM_HANDLE handle, int SelectionLen);
    int GetInputDisplay (IMM_HANDLE handle, char *buf, long buflen);
    int GetSelectDisplay (IMM_HANDLE handle, char *buf, long buflen);

    /* Phrase Item Operations */
    int SetPhraseItem (IMM_HANDLE handle, 
                       u_long n, 
                       char *szCode,
                       char *szPhrase,
                       u_long freq);
    int AddUserPhrase (IMM_HANDLE handle, 
                       char *szCode,
                       char *szPhrase,
                       u_long freq);
    int FlushUserPhrase (IMM_HANDLE handle);
};

#endif

