
/*
**  UNICON - The Console Chinese & I18N 
**  Copyright (c) 1999-2002 Arthur Ma <arthur.ma@turbolinux.com.cn>
**
**  This file is part of UNICON, a console Chinese & I18N
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This library is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
**  Lesser General Public License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
**  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
**
** TLC_MemFile.hpp: TLC Memfile modules
** See the file COPYING directory of this archive
** Author: see CREDITS
*/

#ifndef __MEMFILE_HPP__
#define __MEMFILE_HPP__

#include <stdio.h>
#include <stdlib.h>

typedef char * PSTR;
class TLC_CMemFile
{
private:
   char *buf;
   long max;
   long pos;
   long len;
   bool bAllocate;
public:    
    TLC_CMemFile (char *buf, u_long len, u_long max);
    TLC_CMemFile (u_long max);
    ~TLC_CMemFile ();
    int fseek (long offset, int whence);
    long ftell ();
    size_t fread (void *ptr, size_t size, size_t nmemb);
    size_t  fwrite (void  *ptr,  size_t  size, size_t nmemb);
    void rewind ();
    /* buf operation */
    char *pGetBuf ();
    char *pGetCurrentPos ();
    u_long GetBufLen ();
    u_long GetMax ();
    void SetBufLen (int n);    

    /* operator overload */
    friend TLC_CMemFile & operator >> (TLC_CMemFile &in, long &b);
    friend TLC_CMemFile & operator >> (TLC_CMemFile &in, short &b);
    friend TLC_CMemFile & operator >> (TLC_CMemFile &in, char &b);
    friend TLC_CMemFile & operator >> (TLC_CMemFile &in, PSTR &p);
    friend TLC_CMemFile & operator << (TLC_CMemFile &in, long b);
    friend TLC_CMemFile & operator << (TLC_CMemFile &in, short b);
    friend TLC_CMemFile & operator << (TLC_CMemFile &in, char b);
    friend TLC_CMemFile & operator << (TLC_CMemFile &in, PSTR p);
};

#endif

