/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifdef MODULE
#include <linux/module.h>
#include <linux/fb_doublebyte.h>
#else
#include <mytype.h>
#endif

#include <font_kscm16.h>
// 0x2121 -- 0x7424

static int index_kscm(int left, int right)
{
    int rec;
    left = left & 0x7f;
    right = right & 0x7f;
    rec = (left - 0x21) * (0x7e - 0x21 + 1) + right - 0x21;
    return rec << 5;
}

static int is_hz_left(int c)
{
    if (c < 0x80)
        return 0;
    c = c & 0x7f;
    return (c >= 0x21 && c <= 0x74);
}

static int is_hz_right(int c)
{
    if (c < 0x80)
        return 0;
    c = c & 0x7f;
    return (c >= 21 && c <= 0x7e);
}

#ifdef MODULE
static struct double_byte db_kscm =
#else
struct double_byte db_kscm =
#endif
{
	0,
	"kscm",
	is_hz_left,
	is_hz_right,
	index_kscm,
	16,16,
	max_kscm16,
	font_kscm16
};

#ifdef MODULE
int init_module(void)
{
        if (UniconFontManager == 0)
            return 1;
        if (UniconFontManager->registerfont (XL_DB_KSCM, &db_kscm) == 0)
            return 1;
        return 0;
}
	
void cleanup_module(void)
{
        if (UniconFontManager == 0)
            return;
        UniconFontManager->unregisterfont (XL_DB_KSCM);
}
#endif

