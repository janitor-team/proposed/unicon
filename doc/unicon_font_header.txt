=============================== >8 Begin 8< ================================
J.F.Kennedy once said:
    "Don't ask what your nation can do for you, ask what you can
do for your nation."

Hackers always say:
    "Don't ask what others can do for you, DO IT YOURSELF!"
-------
                    
                    UNICON显示字体头文件的制作方法
                            Hacked by Linuxrat <linuxrat@gnuchina.org>

前言∶
----
    UNICON中的字体使用方式并不是直接调用二进制字体文件，而是做成头文件
方式然后编译成模块．开始时对这种头文件的制作方式比较感兴趣，并就此询问
了于明俭(yumj)老师，得知有专门的工具来制作．知道之后也就没再深究．

    后来要对UNICON做一些改动，包括显示用的字体．因此又重新开始考虑字体
头文件的制作方式来．因为脑子不好的缘故，再也没想起来于明俭老师所说的工
具，于是就自己想办法解决．受unifont.hex和vim/xxd的启发，就想起来了制作
方法，因此在此写出来，希望能够让大家以后更方便使用自己满意的字体模块．
DIY的精神，正是GNU的精神，也是个性化的精神．

相关程序∶
--------
    vim: 编辑器．我一直使用vim，顺手了．:)
    xxd: vim当中经常会用到:%!xxd这个命令，是vim包所有的一个转换工具．
         一般包含在vim-common包中，也有的GNU/Linux发行套件上另外由
         vim-xxd包所提供．

制作过程∶
--------
    01. 取得字体文件．我们以GB2312-80的16点阵字库文件hz16.dat为例．

    02. 确认xxd转换程序已经安装．然后执行以下命令∶
        xxd -c32 -l 291696 -i -u hz16.dat font_gb16.h

        注∶其中291696是hz16.dat的字节数，也正是其所包含的字符字模
            数据所占用的字节数．每个16x16点阵字符需要16x16=256个点
            来表示，每个点使用一个位(bit)来表示，那么表示这样一个
            字符就需要256/8 = 32个字节．font_gb16.h表示需要输出的
            文件，也就是我们所希望得到的头文件．

    03. 使用编辑器对font_gb16.h做一些细小的修改．就是最后一句
        unsigned int hz16_dat_len = 291696 ;
        改为#define hz16_dat_len 291696，并把数组hz16_dat[]定义修改
        为hz16_dat[hz16_dat_len]较为妥当一些．

    04. 然后修改相应的encode-gb16.c内容，正式使用该头文件．请参考
        fonts目录下的相应模块内容．
        
联系方式∶
    倘若您有什么意见或者建议需要同大家分享，欢迎您与我联系．

版权声明∶
    本文遵循GNU FDL(Free Document License)各项条款进行散布．版权持
    有者为UNICON Team．

修订于∶五 08  3 09:43:39 CST 2001
=============================== >8  End  8< ================================
