/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <mytype.h>
#include <filter.hpp>
#include <sfont.hpp>


/* font_type */
#define XL_DB_GB       0
#define XL_DB_BIG5     1
#define XL_DB_JIS      2
#define XL_DB_KSCM     3
#define XL_DB_GBK      4

#define MAX_FONT       5

extern struct double_byte db_big5;
extern struct double_byte db_gb;
extern struct double_byte db_gbk;
extern struct double_byte db_jis;
extern struct double_byte db_kscm;

int coding_int;

static struct double_byte *system_code[MAX_FONT] =
{
    &db_gb, 
    &db_big5, 
    &db_jis, 
    &db_kscm,
    &db_gbk, 
};

bool DoFilter (struct double_byte *coding, 
               char *szTextName, char *szHeadFile)
{
    FILE *fp;
    unsigned char *buf;

    if ((fp = fopen (szTextName, "rb")) == NULL)
    {
        printf ("%s can't open ....\n", szTextName);
        exit (-1);
    }

    fseek (fp, 0, SEEK_END);
    int len = ftell (fp);
    fseek (fp, 0, SEEK_SET);

    buf = (unsigned char *) malloc (len);
    fread (buf, 1, len, fp);
    fclose (fp);

    CSFontFilter *pFontFilter = new CSFontFilter (coding);
    pFontFilter->DoFilter (buf, len);
    bool b = pFontFilter->SaveToFile (szHeadFile, coding_int);
    free (buf);
    return b;
}

void my_usages (char *runfilename)
{
    printf ("%s <--gb | --gbk | --big5 | --jis | --kscm>  \n\
                <--spath path> <filter file> [headfile]\n", runfilename);
    exit (0);
}

int main (int argc, char **argv)
{
    char *szFilename;

    if (argc < 3)
        my_usages (argv[0]);

    if (strcmp (argv[1], "--gb") == 0)
    {
        coding_int = XL_DB_GB;
        szFilename = "sfont_gb16.h";
    }
    else if (strcmp (argv[1], "--gbk") == 0)
    {
        coding_int = XL_DB_GBK;
        szFilename = "sfont_gbk16.h";
    }
    else if (strcmp (argv[1], "--big5") == 0)
    {
        coding_int = XL_DB_BIG5;
        szFilename = "sfont_big5_16.h";
    }
    else if (strcmp (argv[1], "--jis") == 0)
    {
        coding_int = XL_DB_JIS;
        szFilename = "sfont_jis16.h";
    }
    else if (strcmp (argv[1], "--kscm") == 0)
    {
        coding_int = XL_DB_KSCM;
        szFilename = "sfont_kscm16.h";
    }
    else
        my_usages (argv[0]);
    
    char *path;
    if (strcmp (argv[2], "--spath") != 0)
    {
        printf ("bad parameter....\n");
        printf ("--spath expected \n");
    }

    path = argv[3];
    // text filename = argv[4];

    // head file
    if (argc == 6)
        szFilename = argv[5];
        
    struct double_byte *default_code = system_code[coding_int];    
    char fullpath[256];

    sprintf (fullpath, "%s/%s", path, szFilename);
    DoFilter (default_code, argv[4], fullpath);
}

