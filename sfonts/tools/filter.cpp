/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <filter.hpp>

CSFontFilter::CSFontFilter (double_byte *coding)
{
    this->coding = coding;
    pSFont = new  CSFontManager ();
}

CSFontFilter::~CSFontFilter ()
{
    delete pSFont;
}

int CSFontFilter::IsDoubleChar (u_char ch1, u_char ch2)
{
    assert (coding != NULL);
    if (coding->is_left (ch1) == 1 &&
        coding->is_right (ch2) == 1)
        return 1;
    return 0;
}

u_char *CSFontFilter::pGetCharFont (u_char c1, u_char c2)
{
    int index;
    index = coding->font_index(c1, c2);
    if (index >=0 && index < coding->charcount) 
        return coding->font_data +index;
    return NULL;
}

int CSFontFilter::DoFilter (unsigned char *buf, int len)
{
    int i = 0;
    while (i < len - 1)
    {
        if (IsDoubleChar (buf[i], buf[i+1]) == 1)
        {
            u_char *p = pGetCharFont (buf[i], buf[i+1]);
            pSFont->AddCharFont (buf[i], buf[i+1], p);
            if (p == NULL)
                continue;
            i += 2;
        }
        else
            i ++;
    }
    return i;
}

bool CSFontFilter::SaveToFile (char *szFileName, int coding_int)
{
    return pSFont->bSaveToFile (szFileName, coding_int);
}

