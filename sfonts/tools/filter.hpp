/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

#ifndef __FONTFILTER_HPP__
#include <mytype.h>
#include <sfont.hpp>

class CSFontFilter
{
private:
    CSFontManager *pSFont;
    double_byte *coding;
private:
    int IsDoubleChar (u_char ch1, u_char ch2);
    u_char *pGetCharFont (u_char ch1, u_char ch2);
public:
    CSFontFilter (double_byte *coding);
    ~CSFontFilter ();
    int DoFilter (unsigned char *buf, int len);
    bool SaveToFile (char *szFileName, int coding_int);
};

#endif

