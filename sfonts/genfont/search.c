/*
 *
 * UNICON - The Console Chinese & I18N
 * Copyright (c) 1999-2000
 *
 * This file is part of UNICON, a console Chinese & I18N
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * See the file COPYING directory of this archive
 * Author: see CREDITS
 */

static void * My_bsearch (const void *key, const void *base,
                   size_t nmemb, size_t size,
                   int (*compar)(const void *, const void *))
{
    const void *mid_point;
    int  cmp;

    while (nmemb > 0) {
        mid_point = (char *)base + size * (nmemb >> 1);
        if ((cmp = (*compar)(key, mid_point)) == 0)
            return (void *) mid_point;
        if (cmp >= 0) {
            base  = (char *)mid_point + size;
            nmemb = (nmemb - 1) >> 1;
        } else
            nmemb >>= 1;
    }
    return (void *)NULL;
}

static int fcomp (const void *p1, const void *p2)
{
    CharFontInfo *a1 = (CharFontInfo *) p1;
    CharFontInfo *a2 = (CharFontInfo *) p2;
    unsigned int n1, n2;
    n1 = a1->key;
    n2 = a2->key;
    if (n1 > n2)
        return 1;
    else if (n1 == n2)
        return 0;
    else
        return -1;
}

static int SearchInSmallFont (int left, int right)
{
#if 0
    CharFontInfo aa, *p;

    aa.key = (unsigned int) left * (unsigned int) right;
    aa.num = 0;

    p = My_bsearch (&aa, aCharFontInfo, MAX_SFONT, sizeof (aCharFontInfo),
                 fcomp);

    if (p == NULL)
        return 0;
    else
        return p->num;
#endif
    int i;
    unsigned int key = (((unsigned int) left) << 8) + (unsigned int) right;
    for (i = 0;  i < MAX_SFONT; i++)
         if (aCharFontInfo[i].key == key)
             return (i + 1);
    return 0;
}

